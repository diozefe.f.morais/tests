package com.api.votation.votation.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Null;

import java.util.UUID;

public class SessionDto {

    @NotBlank
    private String rulingId;

    @Null
    private String timeSession;

    public UUID getRulingId() {
        return UUID.fromString(rulingId);
    }

    public void setRulingId(String rulingId) {
        this.rulingId = rulingId;
    }

    public String getTimeSession() {
        return timeSession;
    }

    public void setTimeSession(String timeSession) {
        this.timeSession = timeSession;
    }
}

