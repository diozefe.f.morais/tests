package com.api.votation.votation.dtos;

import jakarta.validation.constraints.NotBlank;

public class VoteDto {
    @NotBlank
    private int vote;

    public int getVote() {
        return this.vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }
}
