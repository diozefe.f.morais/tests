package com.api.votation.votation.controllers;

import com.api.votation.votation.dtos.AssociateDto;
import com.api.votation.votation.errors.HttpError;
import com.api.votation.votation.models.AssociateModel;
import com.api.votation.votation.services.AssociateService;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/associate")
public class AssociateController {
    final AssociateService associateService;

    public AssociateController(AssociateService associateService) {
        this.associateService = associateService;
    }

    @PostMapping
    public ResponseEntity<Object> saveAssociate(@RequestBody @Valid AssociateDto associateDto){
        var associateModel = new AssociateModel();
        BeanUtils.copyProperties(associateDto, associateModel);
        associateModel.setRegistrationDate(LocalDateTime.now(ZoneId.of("UTC")));
        return ResponseEntity.status(HttpStatus.CREATED).body(associateService.save(associateModel));
    }

    @GetMapping
    public ResponseEntity<List<AssociateModel>> getAssociates(){
        return ResponseEntity.status(HttpStatus.OK).body(associateService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getAssociateById(@PathVariable(value = "id")UUID id){
        Optional<AssociateModel> associateModelOptional = associateService.findById(id);

        return associateModelOptional.<ResponseEntity<Object>>map(associateModel -> ResponseEntity.status(HttpStatus.OK).body(associateModel))
                .orElseGet(()->ResponseEntity.status(HttpStatus.NOT_FOUND).body(new HttpError("Associate not found", 1)));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateAssociate(@PathVariable(value = "id") UUID id, @RequestBody AssociateDto associateDto){
        Optional<AssociateModel> associateModelOptional = associateService.findById(id);
        if (associateModelOptional.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new HttpError("Associate not found", 1));
        }
        var associateModel = new AssociateModel();
        BeanUtils.copyProperties(associateDto, associateModel);
        associateModel.setId(associateModelOptional.get().getId());
        associateModel.setRegistrationDate(associateModelOptional.get().getRegistrationDate());
        return ResponseEntity.status(HttpStatus.OK).body(associateService.save(associateModel));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteAssociate(@PathVariable(value = "id") UUID id){
        Optional<AssociateModel> associateModelOptional = associateService.findById(id);
        if (associateModelOptional.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new HttpError("Associate not found", 1));
        }
        associateService.delete(associateModelOptional.get());
        return ResponseEntity.status(HttpStatus.OK).body("Associate deleted");
    }
}
