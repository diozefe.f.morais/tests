package com.api.votation.votation.controllers;

import com.api.votation.votation.dtos.SessionDto;
import com.api.votation.votation.dtos.VoteDto;
import com.api.votation.votation.errors.HttpError;
import com.api.votation.votation.models.AssociateModel;
import com.api.votation.votation.models.RulingModel;
import com.api.votation.votation.models.SessionModel;
import com.api.votation.votation.services.AssociateService;
import com.api.votation.votation.services.RulingService;
import com.api.votation.votation.services.SessionService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.ErrorResponseException;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/session")
public class SessionController {
    final SessionService sessionService;
    final RulingService rulingService;
    final AssociateService associateService;
    public SessionController(SessionService sessionService, RulingService rulingService, AssociateService associateService) {
        this.sessionService = sessionService;
        this.rulingService = rulingService;
        this.associateService = associateService;
    }

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody @Valid SessionDto sessionDto){
        Optional<RulingModel> rulingModelOptional = rulingService.findById(sessionDto.getRulingId());

        var sessionModel = new SessionModel();
        var localDateTime = LocalDateTime.now(ZoneId.of("UTC"));

        if (rulingModelOptional.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new HttpError("Ruling not found", 1));
        }

        if (sessionDto.getTimeSession() == null){

            var time = LocalTime.of(0, 1);
            var timeSessionDefault = LocalDateTime.of(localDateTime.toLocalDate(), time);

            sessionModel.setTimeSession(timeSessionDefault);
            sessionModel.setRuling(rulingModelOptional.get());
            sessionModel.setRegistrationDate(localDateTime);

            return ResponseEntity.status(HttpStatus.CREATED).body(sessionService.save(sessionModel));
        }

        var time = LocalTime.of(0, Integer.parseInt(sessionDto.getTimeSession()));
        var timeSession = LocalDateTime.of(localDateTime.toLocalDate(), time);
        sessionModel.setTimeSession(timeSession);
        sessionModel.setRuling(rulingModelOptional.get());
        sessionModel.setRegistrationDate(localDateTime);

        return ResponseEntity.status(HttpStatus.CREATED).body(sessionService.save(sessionModel));
    }

    @PutMapping("/{id}/vote/{associateId}")
    public ResponseEntity<Object> updateSession(
            @PathVariable(value = "id") UUID id,
            @PathVariable(value = "associateId") UUID associateId ,
            @RequestBody VoteDto voteDto)
    {
        Optional<SessionModel> sessionModelOptional = sessionService.findById(id);
        if (sessionModelOptional.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new HttpError("Session not found", 1));
        }

        Optional<AssociateModel> associateModelOptional = associateService.findById(associateId);
        if (associateModelOptional.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new HttpError("Associate not found",1));
        }

        var rulingInSession = sessionModelOptional.get().getRuling();

        if (sessionModelOptional.get().getAssociates().contains(associateModelOptional.get())){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new HttpError("This associate already voted", 3));
        };

        if (voteDto.getVote() == 0){
            rulingInSession.setReprovedCount(rulingInSession.getReprovedCount()+1);
            sessionModelOptional.get().setAssociates(associateModelOptional.get());
            sessionModelOptional.get().setId(sessionModelOptional.get().getId());
            sessionModelOptional.get().setRegistrationDate(sessionModelOptional.get().getRegistrationDate());
            return ResponseEntity.status(HttpStatus.OK).body(sessionService.save(sessionModelOptional.get()));
        } else if (voteDto.getVote() == 1){
            rulingInSession.setApprovedCount(rulingInSession.getApprovedCount()+1);
            sessionModelOptional.get().setAssociates(associateModelOptional.get());
            sessionModelOptional.get().setId(sessionModelOptional.get().getId());
            sessionModelOptional.get().setRegistrationDate(sessionModelOptional.get().getRegistrationDate());
            return ResponseEntity.status(HttpStatus.OK).body(sessionService.save(sessionModelOptional.get()));
        }else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new HttpError("Unexpected value to vote", 2));
        }


    }

}
