package com.api.votation.votation.controllers;

import com.api.votation.votation.dtos.RulingDto;
import com.api.votation.votation.errors.HttpError;
import com.api.votation.votation.models.RulingModel;
import com.api.votation.votation.services.RulingService;
import jakarta.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/ruling")
public class RulingController {
    final RulingService rulingService;

    public RulingController(RulingService rulingService) {
        this.rulingService = rulingService;
    }

    @PostMapping
    public ResponseEntity<Object> saveRuling(@RequestBody @Valid RulingDto rulingDto){
        var rulingModel = new RulingModel();
        BeanUtils.copyProperties(rulingDto, rulingModel);
        rulingModel.setRegistrationDate(LocalDateTime.now(ZoneId.of("UTC")));
        return ResponseEntity.status(HttpStatus.CREATED).body(rulingService.save(rulingModel));
    }

    @GetMapping
    public ResponseEntity<List<RulingModel>> getRulings(){
        return ResponseEntity.status(HttpStatus.OK).body(rulingService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getRulingById(@PathVariable(value = "id") UUID id){
        Optional<RulingModel> rulingModelOptional = rulingService.findById(id);
        return rulingModelOptional.<ResponseEntity<Object>>map(rulingModel -> ResponseEntity.status(HttpStatus.OK).body(rulingModel))
                .orElseGet(() ->
                    ResponseEntity.status(HttpStatus.NOT_FOUND).body(new HttpError("Ruling not found", 1))
                );
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateRuling(@PathVariable(value = "id") UUID id, @RequestBody @Valid RulingDto rulingDto){
        Optional<RulingModel> rulingModelOptional = rulingService.findById(id);
        if (rulingModelOptional.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new HttpError("Ruling not found",1));
        }
        var rulingModel = new RulingModel();
        BeanUtils.copyProperties(rulingDto, rulingModel);
        rulingModel.setId(rulingModelOptional.get().getId());
        rulingModel.setRegistrationDate(rulingModelOptional.get().getRegistrationDate());
        return ResponseEntity.status(HttpStatus.OK).body(rulingService.save(rulingModel));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteRuling(@PathVariable(value = "id") UUID id){
        Optional<RulingModel> rulingModelOptional = rulingService.findById(id);
        if (rulingModelOptional.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new HttpError("Ruling not found",1));
        }
        rulingService.delete(rulingModelOptional.get());
        return ResponseEntity.status(HttpStatus.OK).body("ruling deleted");
    }
}
