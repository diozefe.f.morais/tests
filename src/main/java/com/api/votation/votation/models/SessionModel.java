package com.api.votation.votation.models;

import jakarta.persistence.*;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "TB_SESSION")
public class SessionModel implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @ManyToOne(targetEntity = RulingModel.class)
    @JoinColumn(name = "ruling_id", nullable = false)
    private RulingModel ruling;

    @ManyToMany(targetEntity = AssociateModel.class)
    private List<AssociateModel> associates;

    @Column(nullable = false)
    private LocalDateTime timeSession;

    @Column(nullable = false)
    private LocalDateTime registrationDate;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public RulingModel getRuling() {
        return ruling;
    }

    public void setRuling(RulingModel ruling) {
        this.ruling = ruling;
    }

    public LocalDateTime getTimeSession() {
        return timeSession;
    }

    public void setTimeSession(LocalDateTime timeSession) {
        this.timeSession = timeSession;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public List<AssociateModel> getAssociates() {
        return associates;
    }

    public void setAssociates(AssociateModel associate) {
        this.associates.add(associate);
    }
}
