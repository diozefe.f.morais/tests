package com.api.votation.votation.services;

import com.api.votation.votation.models.RulingModel;
import com.api.votation.votation.repositories.RulingRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RulingService {
    final RulingRepository rulingRepository;
    public RulingService(RulingRepository rulingRepository) {
        this.rulingRepository = rulingRepository;
    }

    @Transactional
    public RulingModel save(RulingModel rulingModel){
        return rulingRepository.save(rulingModel);
    }

    public List<RulingModel> findAll() {
        return rulingRepository.findAll();
    }

    public Optional<RulingModel> findById(UUID id) {
        return  rulingRepository.findById(id);
    }

    public void delete(RulingModel rulingModel) {
        rulingRepository.delete(rulingModel);
    }
}
