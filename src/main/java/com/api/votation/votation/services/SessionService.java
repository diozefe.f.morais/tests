package com.api.votation.votation.services;

import com.api.votation.votation.models.SessionModel;
import com.api.votation.votation.repositories.SessionRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class SessionService {
    final SessionRepository sessionRepository;

    public SessionService(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @Transactional
    public SessionModel save(SessionModel sessionModel){
        return sessionRepository.save(sessionModel);
    }

    public Optional<SessionModel> findById(UUID id) {
        return sessionRepository.findById(id);
    }
}
