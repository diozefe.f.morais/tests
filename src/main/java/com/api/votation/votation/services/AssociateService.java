package com.api.votation.votation.services;

import com.api.votation.votation.models.AssociateModel;
import com.api.votation.votation.repositories.AssociateRepository;
import jakarta.transaction.Transactional;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AssociateService {
    final AssociateRepository associateRepository;

    public AssociateService(AssociateRepository associateRepository) {
        this.associateRepository = associateRepository;
    }

    @Transactional
    public AssociateModel save(AssociateModel associateModel){
        return associateRepository.save(associateModel);
    }

    public List<AssociateModel> findAll() {
        return associateRepository.findAll();
    }

    public Optional<AssociateModel> findById(UUID id) {
        return associateRepository.findById(id);
    }

    public void delete(AssociateModel associateModel) {
        associateRepository.delete(associateModel);
    }
}
