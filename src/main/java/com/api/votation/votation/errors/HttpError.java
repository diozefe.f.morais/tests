package com.api.votation.votation.errors;

public class HttpError extends Base{

    public HttpError(String message, int code) {
        super(message, code);
    }
}
