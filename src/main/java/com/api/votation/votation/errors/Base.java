package com.api.votation.votation.errors;

public class Base {
    public String message;
    public int code;

    public Base(String message, int code) {
        this.message = message;
        this.code = code;
    }
}
