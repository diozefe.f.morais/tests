package com.api.votation.votation.repositories;

import com.api.votation.votation.models.SessionModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SessionRepository extends JpaRepository<SessionModel, UUID> {
}
