package com.api.votation.votation.repositories;

import com.api.votation.votation.models.AssociateModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AssociateRepository extends JpaRepository<AssociateModel, UUID> {
}
