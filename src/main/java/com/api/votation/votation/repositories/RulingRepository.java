package com.api.votation.votation.repositories;

import com.api.votation.votation.models.RulingModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RulingRepository extends JpaRepository<RulingModel, UUID> {

}
